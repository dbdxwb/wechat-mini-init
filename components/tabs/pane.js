// components/tabs/pane.js
Component({

	/**
	 * 定义组件间的关系
	 */
	relations: {
		'./tabs': {
			type: 'parent', // 关联的目标节点应为父节点

			/**
			 * 插入到tabs时执行，target是tabs节点实例对象，触发在attached生命周期之后
			 * @param target
			 */
			linked: function (target) {
				console.log('pane linked: ', target)
			},

			/**
			 * 每次被移动后执行，target是tabs节点实例对象，触发在moved生命周期之后
			 * @param target
			 */
			linkChanged: function (target) {
				console.log('pane linkChanged: ', target)
			},

			/**
			 * 每次被移除时执行，target是tabs节点实例对象，触发在detached生命周期之后
			 * @param target
			 */
			unlinked: function (target) {
				console.log('pane unlinked: ', target)
			}
		}
	},

	/**
	 * 组件的属性列表
	 */
	properties: {

		/**
		 * 标题
		 */
		title: String,
	},

	/**
	 * 组件的初始数据
	 */
	data: {},

	/**
	 * 组件的方法列表
	 */
	methods: {}
});