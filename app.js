//app.js
import './utils/native-extension';
import listener from './utils/listener.js';
import './listener/index.js';

App({
	onLaunch: function (options) {
		options.app = this;
		listener.fire('app.init', options);
	},
	onUnlaunch: function (options) {
		options.app = this;
		listener.fire('app.exit', options);
	},
	onShow: function (options) {
		options.app = this;
		listener.fire('app.show', options);
	},
	onHide: function () {
		listener.fire('app.hide', {app: this});
	},
	onError: function (msg) {
		listener.fire('app.error', {app: this, msg: msg});
	},
	onPageNotFound: function (options) {
		options.app = this;
		listener.fire('app.page.notfound', options);
	},
	globalData: {}
});